package pl.wildeastcoders.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public abstract class PreferencesUtils {
	private static final String IS_INFO_ACCEPTED = "IS_INFO_ACCEPTED";
	private static final String IS_CUSTOM_VALUE_ENABLED = "IS_CUSTOM_VALUE_ENABLED";
	private static final String SELECTED_DPI = "SELECTED_DPI";
	private static final String CUSTOM_DPI = "CUSTOM_DPI";
	private static final String IS_AUTORESTORE_ENABLED = "IS_AUTORESTORE_ENABLED";
	private static final String IS_NOTIFICATION_ENABLED = "IS_NOTIFICATION_ENABLED";
	private static final String SHARED_PREFS = "SHARED_PREFS";

	private static SharedPreferences getSharedPreferences(final Context context) {
		return context.getSharedPreferences(SHARED_PREFS, Context.MODE_MULTI_PROCESS);
	}

	private static String getString(final Context context, final String key) {
		final SharedPreferences sharedPrefs = getSharedPreferences(context);
		return sharedPrefs.getString(key, null);
	}

	private static boolean putString(final Context context, final String key, final String value) {
		final SharedPreferences sharedPrefs = getSharedPreferences(context);
		final Editor sharedPrefsEditor = sharedPrefs.edit();
		sharedPrefsEditor.putString(key, value);
		return sharedPrefsEditor.commit();
	}

	private static boolean putInt(final Context context, final String key, final int value) {
		final SharedPreferences sharedPrefs = getSharedPreferences(context);
		final Editor sharedPrefsEditor = sharedPrefs.edit();
		sharedPrefsEditor.putInt(key, value);
		return sharedPrefsEditor.commit();
	}

	private static int getInt(final Context context, final String key) {
		final SharedPreferences sharedPrefs = getSharedPreferences(context);
		return sharedPrefs.getInt(key, -1);
	}

	private static boolean putLong(final Context context, final String key, final long value) {
		final SharedPreferences sharedPrefs = getSharedPreferences(context);
		final Editor sharedPrefsEditor = sharedPrefs.edit();
		sharedPrefsEditor.putLong(key, value);
		return sharedPrefsEditor.commit();
	}

	private static long getLong(final Context context, final String key) {
		final SharedPreferences sharedPrefs = getSharedPreferences(context);
		return sharedPrefs.getLong(key, -1);
	}

	private static boolean putBoolean(final Context context, final String key, final boolean value) {
		final SharedPreferences sharedPrefs = getSharedPreferences(context);
		final Editor sharedPrefsEditor = sharedPrefs.edit();
		sharedPrefsEditor.putBoolean(key, value);
		return sharedPrefsEditor.commit();
	}

	private static boolean getBoolean(final Context context, final String key) {
		final SharedPreferences sharedPrefs = getSharedPreferences(context);
		return sharedPrefs.getBoolean(key, false);
	}

	public static boolean containsKey(final Context context, final String key) {
		final SharedPreferences sharedPrefs = getSharedPreferences(context);
		return sharedPrefs.contains(key);
	}

	public static boolean getIsNotificationEnabled(final Context context) {
		return getBoolean(context, IS_NOTIFICATION_ENABLED);
	}

	public static void setIsNotificationEnabled(final Context context, boolean value) {
		putBoolean(context, IS_NOTIFICATION_ENABLED, value);
	}

	public static boolean getIsAutorestoreEnabled(final Context context) {
		return getBoolean(context, IS_AUTORESTORE_ENABLED);
	}

	public static void setIsAutorestoreEnabled(final Context context, boolean value) {
		putBoolean(context, IS_AUTORESTORE_ENABLED, value);
	}

	public static boolean getIsCustomValueEnabled(final Context context) {
		return getBoolean(context, IS_CUSTOM_VALUE_ENABLED);
	}

	public static void setIsCustomValueEnabled(final Context context, boolean value) {
		putBoolean(context, IS_CUSTOM_VALUE_ENABLED, value);
	}

	public static int getSelectedDPI(final Context context) {
		return getInt(context, SELECTED_DPI);
	}

	public static void setSelectedDPI(final Context context, int value) {
		putInt(context, SELECTED_DPI, value);
	}

	public static int getCustomDPI(final Context context) {
		return getInt(context, CUSTOM_DPI);
	}

	public static void setCustomDPI(final Context context, int value) {
		putInt(context, CUSTOM_DPI, value);
	}

	public static boolean getIsInfoAccepted(final Context context) {
		return getBoolean(context, IS_INFO_ACCEPTED);
	}

	public static void setIsInfoAccepted(final Context context, boolean value) {
		putBoolean(context, IS_INFO_ACCEPTED, value);
	}
}
