package pl.wildeastcoders.utils;

import java.util.ArrayList;

import pl.wildeastcoders.dpichanger.R;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;

public abstract class SystemMessageUtils {
	public static AlertDialog showAlertDialog(Context context, String title, String message, String positiveButtonText,
			DialogInterface.OnClickListener positiveListener, String negativeButtonText, DialogInterface.OnClickListener negativeListener,
			String neutralButtonText, DialogInterface.OnClickListener neutralListener, boolean isCancelable) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

		// set title
		alertDialogBuilder.setTitle(title).setMessage(message).setCancelable(isCancelable);

		if (!TextUtils.isEmpty(positiveButtonText)) {
			alertDialogBuilder.setPositiveButton(positiveButtonText, positiveListener);
		}
		if (!TextUtils.isEmpty(negativeButtonText)) {
			alertDialogBuilder.setNegativeButton(negativeButtonText, negativeListener);
		}
		if (!TextUtils.isEmpty(neutralButtonText)) {
			alertDialogBuilder.setNeutralButton(neutralButtonText, neutralListener);
		}
		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
		return alertDialog;
	}

	public static AlertDialog showAlertWithCustomView(Context context, String title, View view, String positiveButtonText,
			DialogInterface.OnClickListener positiveListener, String negativeButtonText, DialogInterface.OnClickListener negativeListener,
			String neutralButtonText, DialogInterface.OnClickListener neutralListener, boolean isCancelable) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

		// set title
		alertDialogBuilder.setTitle(title).setCancelable(isCancelable);

		if (!TextUtils.isEmpty(positiveButtonText)) {
			alertDialogBuilder.setPositiveButton(positiveButtonText, positiveListener);
		}
		if (!TextUtils.isEmpty(negativeButtonText)) {
			alertDialogBuilder.setNegativeButton(negativeButtonText, negativeListener);
		}
		if (!TextUtils.isEmpty(neutralButtonText)) {
			alertDialogBuilder.setNeutralButton(neutralButtonText, neutralListener);
		}
		alertDialogBuilder.setView(view);
		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
		return alertDialog;
	}

	public static void showNotification(Context context, int notificationId, PendingIntent pendingIntent, String notificationText) {
		Notification myNotification = new NotificationCompat.Builder(context).setContentTitle("DPI Changer Info").setContentText(notificationText)
				.setTicker("DPI Changer Info. " + notificationText).setWhen(System.currentTimeMillis()).setDefaults(Notification.DEFAULT_SOUND)
				.setAutoCancel(true).setSmallIcon(R.drawable.ic_launcher).setContentIntent(pendingIntent).setAutoCancel(true).build();
		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(notificationId, myNotification);
	}

	public static AlertDialog showListDialog(final Context context, ArrayList<String> items, String title, String negativeText,
			DialogInterface.OnClickListener itemClickListener) {
		AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
		builderSingle.setIcon(R.drawable.ic_launcher);
		builderSingle.setTitle(title);
		final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.select_dialog_singlechoice, items);
		builderSingle.setNegativeButton(!TextUtils.isEmpty(negativeText) ? negativeText : "Cancel", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});

		builderSingle.setAdapter(arrayAdapter, itemClickListener);
		AlertDialog dialog = builderSingle.create();
		dialog.show();
		return dialog;
	}
}
