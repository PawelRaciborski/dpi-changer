package pl.wildeastcoders.utils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import org.apache.commons.io.FileUtils;

import pl.wildeastcoders.dpichanger.R;
import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.TextView;

public class DpiExecutor extends ExecuteAsRootBase {

	private static final String _BACKUP = "_backup_";
	private static final String PATH = Environment.getRootDirectory().getPath();
	private static final String BUILD_PROP = "build.prop";
	private static final String TAG_LCD_DENSITY = "ro.sf.lcd_density";
	private String fileName;
	private String fromPath;
	private String toPath;
	private int dpi;
	private ExecutionMode mode;
	private String backupName;
	public static final int[] DPI_VALUES_ARRAY = { 240, 270, 360 };
	private static final SimpleDateFormat formatter = new SimpleDateFormat("dd_MM_yyyy_hhmmss", Locale.getDefault());

	public enum ExecutionMode {
		COPY_FILE, CHANGE_FILE, REBOOT, MAKE_BACKUP, RESTORE_BACKUP
	}

	public String getFromPath() {
		return fromPath;
	}

	public void setFromPath(String fromPath) {
		this.fromPath = fromPath;
	}

	public String getToPath() {
		return toPath;
	}

	public void setToPath(String toPath) {
		this.toPath = toPath;
	}

	public int getDpi() {
		return dpi;
	}

	public void setDpi(int dpi) {
		this.dpi = dpi;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public ExecutionMode getMode() {
		return mode;
	}

	public void setMode(ExecutionMode mode) {
		this.mode = mode;
	}

	public DpiExecutor(String fileName, String fromPath, String toPath, int dpi) {
		super();
		this.fileName = fileName;
		this.fromPath = fromPath;
		this.toPath = toPath;
		this.dpi = dpi;
		this.mode = ExecutionMode.COPY_FILE;
		this.backupName = null;
	}

	public DpiExecutor(String toPath, int dpi) {
		super();
		this.fileName = BUILD_PROP;
		this.fromPath = PATH;
		this.toPath = toPath;
		this.dpi = dpi;
		this.mode = ExecutionMode.COPY_FILE;
		this.backupName = null;
	}

	public static String getBackupName(){
		return  getBackupName(BUILD_PROP);//BUILD_PROP + _BACKUP + formatter.format(new Date());
	}
	
	public static String getBackupName(String filename){
		return filename + _BACKUP + formatter.format(new Date());
	}
	
	@Override
	protected ArrayList<String> getCommandsToExecute() {

		ArrayList<String> commands = new ArrayList<String>();
		if (mode == ExecutionMode.COPY_FILE) {
			// mount /system
			 commands.add("mount -o remount rw /system/");
			 commands.add("mkdir -p " + this.toPath);
			// copy build.prop to change
			commands.add("cp " + this.fromPath + "/" + this.fileName + " " + this.toPath + "/" + this.fileName + "2");
			// make build.prop backup
			commands.add("cp " + this.fromPath + "/" + this.fileName + " " + this.toPath + "/" + getBackupName());
		} else if (mode == ExecutionMode.CHANGE_FILE) {
			// mount /system
			commands.add("mount -o remount rw /system/");
			// write new build.prop file
			commands.add("cp " + this.toPath + "/" + this.fileName + "2" + " " + this.fromPath + "/" + this.fileName);
		} else if (mode == ExecutionMode.REBOOT) {
			commands.add("reboot");
		} else if (mode == ExecutionMode.MAKE_BACKUP) {
			// make build.prop backup
			if (!TextUtils.isEmpty(this.backupName)) {
				commands.add("mkdir -p " + this.toPath);
				commands.add("cp " + this.fromPath + "/" + this.fileName + " " + this.toPath + "/" + this.backupName);
			}
		} else if (mode == ExecutionMode.RESTORE_BACKUP) {
			if (!TextUtils.isEmpty(this.backupName)) {
				// mount /system
				commands.add("mount -o remount rw /system/");
				// write backup build.prop file
				commands.add("cp " + this.toPath + "/" + this.backupName + " " + this.fromPath + "/" + this.fileName);
			}
		}
		return commands;
	}

	private void changeFileDPI() {
		try {
			String from = this.toPath + "/" + this.fileName + "2";//
			File f = new File(from);
			String str = FileUtils.readFileToString(f, "utf-8");
			String[] splitLines = str.split("\n");
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < splitLines.length; i++) {
				if (splitLines[i].startsWith(TAG_LCD_DENSITY)) {//
					String[] tmp = splitLines[i].split("=");
					tmp[1] = Integer.toString(this.dpi);
					splitLines[i] = tmp[0] + "=" + tmp[1];
				}
				sb.append(splitLines[i]).append("\n");
			}
			FileUtils.writeStringToFile(f, sb.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void modifyFile() {
		this.setMode(ExecutionMode.COPY_FILE);
		this.execute();
		changeFileDPI();
		this.setMode(ExecutionMode.CHANGE_FILE);
		this.execute();
	}

	public static int readCurrentDPI() {
		String fromPath = PATH + "/" + BUILD_PROP;
		try {
			File f = new File(fromPath);
			String str = FileUtils.readFileToString(f, "utf-8");
			String[] splitLines = str.split("\n");
			for (int i = 0; i < splitLines.length; i++) {
				if (splitLines[i].startsWith(TAG_LCD_DENSITY)) {
					String[] tmp = splitLines[i].split("=");
					return Integer.parseInt(tmp[1].trim());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	public void rebootDevice() {
		this.setMode(ExecutionMode.REBOOT);
		this.execute();
	}

	public void makeBackup(String fileName) {
		this.backupName = fileName;
		this.setMode(ExecutionMode.MAKE_BACKUP);
		this.execute();
		this.backupName = null;
	}

	public void restoreBackup(String fileName) {
		this.backupName = fileName;
		this.setMode(ExecutionMode.RESTORE_BACKUP);
		this.execute();
		this.backupName = null;
	}

	public static Integer tryParseDPI(String dpi) {
		try {
			return Integer.parseInt(dpi);
		} catch (Exception e) {
			return null;
		}
	}

	public static String validateDPI(Context context, String dpi) {
		Integer res = tryParseDPI(dpi);
		if (res != null) {
			if (res < 100) {
				return context.getString(R.string.dpi_is_to_low);
			} else if (res > 500) {
				return context.getString(R.string.dpi_is_to_high);
			} else
				return null;
		} else {
			return context.getString(R.string.wrong_value);
		}
	}
	
	public static ArrayList<String>getFileNames(String path){
		//String path = Environment.getExternalStorageDirectory().toString()+"/Pictures";
		try{
		Log.d("Files", "Path: " + path);
		File f = new File(path);        
		File file[] = f.listFiles();
		Log.d("Files", "Size: "+ file.length);
		ArrayList<String> items = new ArrayList<String>(file.length);
		
		for (int i=0; i < file.length; i++)
		{
		    Log.d("Files", "FileName:" + file[i].getName());
		    if(file[i].getName().toLowerCase().contains("backup")){
		    	items.add(file[i].getName());
		    }
		}
		return items;
		}
		catch(Exception e){
			return null;
		}
	}
}
