package pl.wildeastcoders.dpichanger;

import pl.wildeastcoders.utils.DpiExecutor;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public abstract class BaseAsyncTask extends AsyncTask<Object,Object,Object> {

	protected String title;
	protected String subtitle;
	protected ProgressDialog progressDialog;
	protected AsyncTaskChangeListener listener;
	protected Context context;
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		progressDialog= new ProgressDialog(context);
		progressDialog.setTitle("Please wait...");
		progressDialog.setMessage("Changing build.prop file");
		progressDialog.setCancelable(false);
		progressDialog.show();
		//executor = new DpiExecutor(toPath, dpi);
	}
	
	@Override
	protected void onPostExecute(Object result) {
		super.onPostExecute(result);
		progressDialog.dismiss();
		if (listener != null){
			if (result == null) {
				listener.onFinished(result);
			} else {
				listener.onError(result);
			}
		}
	}
	
//	@Override
//	protected Object doInBackground(Object... params) {
//		// TODO Auto-generated method stub
//		return null;
//	}

}
