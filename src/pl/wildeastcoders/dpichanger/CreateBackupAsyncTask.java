package pl.wildeastcoders.dpichanger;

import pl.wildeastcoders.utils.DpiExecutor;
import pl.wildeastcoders.utils.PreferencesUtils;
import android.content.Context;

public class CreateBackupAsyncTask extends BaseAsyncTask {
	
	private String backupName;
	private String backupDir;
	private DpiExecutor executor;
	
	public CreateBackupAsyncTask(String backupName, Context context, String backupDir, AsyncTaskChangeListener listener) {
		super();
		this.backupName = backupName;
		this.context = context;
		this.backupDir = backupDir;
		this.listener = listener;
		this.title = context.getString(R.string.please_wait_);
		this.subtitle = context.getString(R.string.creating_backup_file);
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		executor = new DpiExecutor(this.backupDir, PreferencesUtils.getSelectedDPI(context));
	}
	
	@Override
	protected void onPostExecute(Object result) {
		super.onPostExecute(result);
	}
	
	@Override
	protected Object doInBackground(Object... arg0) {
		try {
			executor.makeBackup(backupName);
		} catch (Exception e) {
			return e.getMessage();
		}
		return null;
	}

}
