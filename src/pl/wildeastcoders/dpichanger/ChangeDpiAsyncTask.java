package pl.wildeastcoders.dpichanger;

import pl.wildeastcoders.utils.DpiExecutor;
import android.content.Context;

public class ChangeDpiAsyncTask extends BaseAsyncTask {

	private int dpi;
	private String toPath;
	private DpiExecutor executor;

	public ChangeDpiAsyncTask(int dpi, Context context, String toPath,
			AsyncTaskChangeListener listener) {
		super();
		this.dpi = dpi;
		this.context = context;
		this.toPath = toPath;
		this.listener = listener;
		this.title= context.getString(R.string.please_wait_);
		this.subtitle=context.getString(R.string.changing_build_prop_file);
	}

	public int getDpi() {
		return dpi;
	}

	public void setDpi(int dpi) {
		this.dpi = dpi;
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public String getToPath() {
		return toPath;
	}

	public void setToPath(String toPath) {
		this.toPath = toPath;
	}

	@Override
	protected void onPreExecute() {
		
		super.onPreExecute();
		executor = new DpiExecutor(toPath, dpi);
	}

	@Override
	protected void onPostExecute(Object result) {
		super.onPostExecute(result);
	}

	@Override
	protected Object doInBackground(Object... arg0) {
		try {
			executor.modifyFile();
		} catch (Exception e) {
			return e.getMessage();
		}
		return null;
	}

}
