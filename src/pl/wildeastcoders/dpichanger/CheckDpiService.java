package pl.wildeastcoders.dpichanger;

import pl.wildeastcoders.utils.Constants;
import pl.wildeastcoders.utils.DpiExecutor;
import pl.wildeastcoders.utils.PreferencesUtils;
import pl.wildeastcoders.utils.SystemMessageUtils;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;

public class CheckDpiService extends IntentService {
	
	private static final int MY_NOTIFICATION_ID = 1;
	NotificationManager notificationManager;
	Notification myNotification;

	public CheckDpiService() {
		super("ReminderService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		int currentDpi = DpiExecutor.readCurrentDPI();
		String notificationText;
		Intent mainActivityIntent = new Intent(this, MainActivity.class);
		boolean dpiChanged = currentDpi != PreferencesUtils.getSelectedDPI(this);
		if (currentDpi > 0) {
	
			if(dpiChanged && PreferencesUtils.getIsAutorestoreEnabled(this)){
				restoreDPI();
				notificationText = String.format(getString(R.string.dpi_has_been_restored_to_d_click_this_notification_to_reboot_), PreferencesUtils.getSelectedDPI(this));
				mainActivityIntent.putExtra(MainActivity.NOTIFICATION_OPEN_FLAG,
						MainActivity.NOTIFICATION_REBOOT);
			}else if (dpiChanged) {
				notificationText = String.format(getString(R.string.current_dpi_d_selected_dpi_d), currentDpi,PreferencesUtils.getSelectedDPI(this)); 
				mainActivityIntent.putExtra(MainActivity.NOTIFICATION_OPEN_FLAG,
						MainActivity.NOTIFICATION_SHOW_DIALOG);
			} else {
				return;
			}
		} else {
			notificationText = getString(R.string.error_occured_while_reading_build_prop_file);
		}
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
				mainActivityIntent, PendingIntent.FLAG_CANCEL_CURRENT);

		SystemMessageUtils.showNotification(this, MY_NOTIFICATION_ID, pendingIntent, notificationText);

	}
	
	private void restoreDPI(){
		DpiExecutor executor = new DpiExecutor(Constants.APP_DIRECTORY, PreferencesUtils.getSelectedDPI(this));
		executor.modifyFile();
	}

}
