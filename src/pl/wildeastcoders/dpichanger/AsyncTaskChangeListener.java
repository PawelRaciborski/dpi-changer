package pl.wildeastcoders.dpichanger;

public interface AsyncTaskChangeListener {
	void onFinished(Object result);

	void onCanceled();

	void onStarted();

	void onError(Object error);
}
