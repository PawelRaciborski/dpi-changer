package pl.wildeastcoders.dpichanger;

import pl.wildeastcoders.utils.PreferencesUtils;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

public class BootCompletedReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent inte) {
		if (PreferencesUtils.getIsNotificationEnabled(context)) {
			Intent serviceIntent = new Intent(context, CheckDpiService.class);
			// serviceIntent.putExtra("caller", "RebootReceiver");
			context.startService(serviceIntent);
		}

	}

}
