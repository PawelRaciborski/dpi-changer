package pl.wildeastcoders.dpichanger;

import java.util.ArrayList;
import java.util.List;
import pl.wildeastcoders.utils.Constants;
import pl.wildeastcoders.utils.DpiExecutor;
import pl.wildeastcoders.utils.ExecuteAsRootBase;
import pl.wildeastcoders.utils.PreferencesUtils;
import pl.wildeastcoders.utils.SystemMessageUtils;
import android.app.Activity;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	public static final String NOTIFICATION_OPEN_FLAG = "NOTIFICATION_OPEN_FLAG";
	public static final int NOTIFICATION_SHOW_DIALOG = 0;
	public static final int NOTIFICATION_REBOOT = 1;
	private PlaceholderFragment fragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			fragment = new PlaceholderFragment();
			getFragmentManager().beginTransaction().add(R.id.container, fragment).commit();
		}

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Boolean canRoot = ExecuteAsRootBase.canRunRootCommands();
		if (!canRoot) {
			SystemMessageUtils.showAlertDialog(this, getString(R.string.warning),
					getString(R.string.looks_like_that_app_don_t_have_root_access_without_this_root_dpi_changer_won_t_work_), null, null, null, null,
					getString(R.string.ok), null, true);
		} else if (!PreferencesUtils.getIsInfoAccepted(this)) {
			SystemMessageUtils
					.showAlertDialog(
							this,
							getString(R.string.info),
							getString(R.string.this_app_performs_commands_as_root_and_modifies_build_prop_file_this_potentially_can_harm_your_device_but_it_shouldn_t_you_are_using_this_app_at_your_own_risk_),
							null, null, null, null, getString(R.string.ok), new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									PreferencesUtils.setIsInfoAccepted(MainActivity.this, true);

								}
							}, false);

		}
		processIntent(getIntent());

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);// Menu Resource, Menu
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_about:
			View view = getLayoutInflater().inflate(R.layout.about, null);
			SystemMessageUtils.showAlertWithCustomView(this, getString(R.string.about), view, "OK", null, null, null, null, null, true);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		processIntent(intent);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	private void processIntent(Intent intent) {
		if (intent != null) {
			Bundle bun = intent.getExtras();
			if (bun != null && bun.containsKey(NOTIFICATION_OPEN_FLAG)) {
				int flag = bun.getInt(NOTIFICATION_OPEN_FLAG);
				if (flag == NOTIFICATION_SHOW_DIALOG) {
					SystemMessageUtils.showAlertDialog(this, getString(R.string.dpi_changed), String.format(
							getString(R.string.app_has_detected_that_your_current_dpi_is_d_and_your_prefered_dpi_is_d_would_you_like_to_restore_it_),
							DpiExecutor.readCurrentDPI(), PreferencesUtils.getSelectedDPI(this)), getString(R.string.yes),
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface arg0, int arg1) {
									fragment.performChangeDpi();
								}
							}, getString(R.string.no), null, null, null, false);
				} else if (flag == NOTIFICATION_REBOOT) {
					DpiExecutor executor = new DpiExecutor(null, 0);
					executor.rebootDevice();
				}
			}
		}
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		private Spinner spinDpi;
		private Button btnSubmit;

		private Button btnSendBroadcast;
		private Button btnReboot;
		private Button btnRestoreBackup;
		private Button btnBackup;
		private TextView txtMainLabel;
		private CheckBox checkBoxCustomeValue;
		private CheckBox checkBoxNotifyChanges;
		private CheckBox checkBoxAutoRestore;
		private EditText editCustomValue;

		public PlaceholderFragment() {
		}

		private void performChangeDpi() {
			ChangeDpiAsyncTask task = new ChangeDpiAsyncTask(PreferencesUtils.getSelectedDPI(getActivity()), getActivity(), Constants.APP_DIRECTORY,
					new AsyncTaskChangeListener() {

						@Override
						public void onStarted() {
							// Nothing to do here
						}

						@Override
						public void onFinished(Object result) {

							SystemMessageUtils.showAlertDialog(getActivity(), getActivity().getString(R.string.success_),

							String.format(getActivity().getString(R.string.dpi_changed_to_d_would_you_like_to_reboot_your_device_to_apply_changes_),
									PreferencesUtils.getSelectedDPI(getActivity())), getActivity().getString(R.string.yes),
									new DialogInterface.OnClickListener() {

										@Override
										public void onClick(DialogInterface dialog, int which) {
											DpiExecutor executor = new DpiExecutor(null, 0);
											executor.rebootDevice();
										}
									}, getActivity().getString(R.string.no), new DialogInterface.OnClickListener() {

										@Override
										public void onClick(DialogInterface dialog, int which) {
											Toast.makeText(
													PlaceholderFragment.this.getActivity(),
													String.format(
															getActivity().getString(
																	R.string.dpi_changed_to_d_please_restart_your_device_to_apply_changes_),
															PreferencesUtils.getSelectedDPI(getActivity())), Toast.LENGTH_LONG).show();
											txtMainLabel.setText(String.format(getActivity().getString(R.string.current_dpi_d),
													DpiExecutor.readCurrentDPI()));

										}
									}, null, null, false);
						}

						@Override
						public void onError(Object error) {
							showErrorToast(error);
						}

						@Override
						public void onCanceled() {
							showCancelToast();

						}

					});
			task.execute();
		}

		private void showErrorToast(Object error) {
			if (error instanceof String)
				Toast.makeText(getActivity(), String.format(getActivity().getString(R.string.error_s), error.toString()), Toast.LENGTH_LONG).show();
			txtMainLabel.setText(String.format(getActivity().getString(R.string.current_dpi_d), DpiExecutor.readCurrentDPI()));
		}

		private void showCancelToast() {
			Toast.makeText(getActivity(), R.string.canceled_, Toast.LENGTH_LONG).show();
			txtMainLabel.setText(String.format(getActivity().getString(R.string.current_dpi_d), DpiExecutor.readCurrentDPI()));
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container, false);
			spinDpi = (Spinner) rootView.findViewById(R.id.spin_dpi_value);
			List<Integer> intList = new ArrayList<Integer>();
			int selectedIndex = 0;
			for (int i = 0; i < DpiExecutor.DPI_VALUES_ARRAY.length; i++) {
				intList.add(DpiExecutor.DPI_VALUES_ARRAY[i]);
				if (DpiExecutor.DPI_VALUES_ARRAY[i] == PreferencesUtils.getSelectedDPI(getActivity())) {
					selectedIndex = i;
				}
			}
			ArrayAdapter<Integer> dataAdapter = new ArrayAdapter<Integer>(this.getActivity(), android.R.layout.simple_spinner_item, intList);
			dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinDpi.setAdapter(dataAdapter);

			spinDpi.setSelection(selectedIndex);
			spinDpi.setEnabled(!PreferencesUtils.getIsCustomValueEnabled(getActivity()));
			btnReboot = (Button) rootView.findViewById(R.id.btn_reboot);
			btnReboot.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					DpiExecutor executor = new DpiExecutor(null, 0);
					executor.rebootDevice();
				}
			});
			btnSubmit = (Button) rootView.findViewById(R.id.btn_submit);
			btnSubmit.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					editCustomValue.setError(null);
					int selectedDpi;
					if (checkBoxCustomeValue.isChecked()) {
						Integer result = DpiExecutor.tryParseDPI(editCustomValue.getText().toString());
						if (result != null) {
							selectedDpi = result;
						} else {
							editCustomValue.setError(getActivity().getString(R.string.wrong_value));
							return;
						}
					} else {
						selectedDpi = (Integer) spinDpi.getSelectedItem();
					}
					PreferencesUtils.setSelectedDPI(getActivity(), selectedDpi);
					performChangeDpi();
				}
			});
			btnRestoreBackup = (Button) rootView.findViewById(R.id.btn_restore_backup);
			btnRestoreBackup.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					final ArrayList<String> items = DpiExecutor.getFileNames(Constants.APP_DIRECTORY);
					if (items == null || items.size() == 0) {
						Toast.makeText(getActivity(), R.string.no_backup_files, Toast.LENGTH_LONG).show();
						return;
					}
					SystemMessageUtils.showListDialog(getActivity(), items, getActivity().getString(R.string.select_backup_file_), getActivity().getString(R.string.cancel),
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									final String fileName = items.get(which);
									RestoreBackupAsyncTask task = new RestoreBackupAsyncTask(fileName, getActivity(), Constants.APP_DIRECTORY,
											new AsyncTaskChangeListener() {

												@Override
												public void onFinished(Object result) {

													SystemMessageUtils.showAlertDialog(
															getActivity(),
															getActivity().getString(R.string.success_),

															String.format(
																	getActivity()
																			.getString(
																					R.string.file_s_has_been_restored_would_you_like_to_reboot_device_to_apply_changes_),
																	fileName), getActivity().getString(R.string.yes),
															new DialogInterface.OnClickListener() {

																@Override
																public void onClick(DialogInterface dialog, int which) {
																	DpiExecutor executor = new DpiExecutor(null, 0);
																	executor.rebootDevice();
																}
															}, getActivity().getString(R.string.no), new DialogInterface.OnClickListener() {

																@Override
																public void onClick(DialogInterface dialog, int which) {
																	Toast.makeText(
																			PlaceholderFragment.this.getActivity(),
																			String.format(
																					getActivity()
																							.getString(
																									R.string.dpi_changed_to_d_please_restart_your_device_to_apply_changes_),
																					DpiExecutor.readCurrentDPI()), Toast.LENGTH_LONG).show();
																	txtMainLabel.setText(String.format(getActivity()
																			.getString(R.string.current_dpi_d), DpiExecutor.readCurrentDPI()));

																}
															}, null, null, false);

												}

												@Override
												public void onCanceled() {
													showCancelToast();

												}

												@Override
												public void onStarted() {
													// Nothing to do here

												}

												@Override
												public void onError(Object error) {
													showErrorToast(error);

												}
											});
									task.execute();

								}
							});
				}
			});
			txtMainLabel = (TextView) rootView.findViewById(R.id.txt_main_label);
			txtMainLabel.setText(String.format(getActivity().getString(R.string.current_dpi_d), DpiExecutor.readCurrentDPI()));

			editCustomValue = (EditText) rootView.findViewById(R.id.edit_custom_value);
			editCustomValue.setEnabled(PreferencesUtils.getIsCustomValueEnabled(getActivity()));
			if (PreferencesUtils.getCustomDPI(getActivity()) > 0) {
				editCustomValue.setText(Integer.toString(PreferencesUtils.getCustomDPI(getActivity())));
			}
			editCustomValue.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// Nothing to do here.

				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
					editCustomValue.setError(null);

				}

				@Override
				public void afterTextChanged(Editable s) {
					String val = DpiExecutor.validateDPI(PlaceholderFragment.this.getActivity(), s.toString());
					editCustomValue.setError(val);
					if (val == null) {
						PreferencesUtils.setCustomDPI(getActivity(), DpiExecutor.tryParseDPI(s.toString()));
					}
				}
			});
			checkBoxCustomeValue = (CheckBox) rootView.findViewById(R.id.checkbox_custom_value);
			checkBoxCustomeValue.setChecked(PreferencesUtils.getIsCustomValueEnabled(getActivity()));
			checkBoxCustomeValue.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					editCustomValue.setError(null);
					editCustomValue.setEnabled(isChecked);
					spinDpi.setEnabled(!isChecked);
					PreferencesUtils.setIsCustomValueEnabled(getActivity(), isChecked);
				}
			});
			checkBoxNotifyChanges = (CheckBox) rootView.findViewById(R.id.checkbox_notify_value_changed);
			checkBoxNotifyChanges.setChecked(PreferencesUtils.getIsNotificationEnabled(getActivity()));

			checkBoxNotifyChanges.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					// checkBoxAutoRestore.setEnabled(isChecked);
					if (!isChecked) {
						checkBoxAutoRestore.setChecked(false);
					}
					checkBoxAutoRestore.setEnabled(isChecked);
					PreferencesUtils.setIsNotificationEnabled(getActivity(), isChecked);
				}
			});

			checkBoxAutoRestore = (CheckBox) rootView.findViewById(R.id.checkbox_auto_restor);
			checkBoxAutoRestore.setEnabled(PreferencesUtils.getIsNotificationEnabled(getActivity()));
			checkBoxAutoRestore.setChecked(PreferencesUtils.getIsAutorestoreEnabled(getActivity()));
			checkBoxAutoRestore.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					PreferencesUtils.setIsAutorestoreEnabled(getActivity(), isChecked);
				}
			});
			btnSendBroadcast = (Button) rootView.findViewById(R.id.btn_send_broadcast);
			btnSendBroadcast.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intnet = new Intent("pl.wildeastcoders.WEC_TEST_ACTION");
					getActivity().sendBroadcast(intnet);

				}
			});

			btnBackup = (Button) rootView.findViewById(R.id.btn_backup);
			btnBackup.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					final String name = DpiExecutor.getBackupName();
					CreateBackupAsyncTask task = new CreateBackupAsyncTask(name, getActivity(), Constants.APP_DIRECTORY,
							new AsyncTaskChangeListener() {

								@Override
								public void onStarted() {
									// TODO Auto-generated method stub

								}

								@Override
								public void onFinished(Object result) {
									Toast.makeText(getActivity(), String.format(getActivity().getString(R.string.file_s_has_been_created_), name),
											Toast.LENGTH_LONG).show();

								}

								@Override
								public void onError(Object error) {
									showErrorToast(error);

								}

								@Override
								public void onCanceled() {
									showCancelToast();

								}
							});
					task.execute();
				}
			});

			return rootView;
		}
	}

}
