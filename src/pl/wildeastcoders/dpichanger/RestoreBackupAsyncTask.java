package pl.wildeastcoders.dpichanger;

import pl.wildeastcoders.utils.DpiExecutor;
import pl.wildeastcoders.utils.PreferencesUtils;
import android.content.Context;

public class RestoreBackupAsyncTask extends BaseAsyncTask {

	private String backupName;
	private String backupDir;
	private DpiExecutor executor;

	public RestoreBackupAsyncTask(String backupName, Context context, String backupDir, AsyncTaskChangeListener listener) {
		super();
		this.backupName = backupName;
		this.context = context;
		this.backupDir = backupDir;
		this.listener = listener;
		this.title = context.getString(R.string.please_wait_);
		this.subtitle = context.getString(R.string.changing_build_prop_file);
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		executor = new DpiExecutor(this.backupDir, PreferencesUtils.getSelectedDPI(context));
	}

	@Override
	protected void onPostExecute(Object result) {
		super.onPostExecute(result);
	}

	@Override
	protected Object doInBackground(Object... arg0) {
		try {
			executor.restoreBackup(backupName);
			PreferencesUtils.setSelectedDPI(context, DpiExecutor.readCurrentDPI());
		} catch (Exception e) {
			return e.getMessage();
		}
		return null;
	}

	public String getBackupName() {
		return backupName;
	}

	public void setBackupName(String backupName) {
		this.backupName = backupName;
	}

	public String getBackupDir() {
		return backupDir;
	}

	public void setBackupDir(String backupDir) {
		this.backupDir = backupDir;
	}

}
